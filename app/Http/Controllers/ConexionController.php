<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 
class ConexionController extends Controller
{
    
    public function conexion(){
        $conexion= oci_new_connect("RICX","12345","127.0.0.1/xe"); // UsuarioBD, ContraseñaBD y Host

        if(!$conexion){
            $err = oci_error();
            echo $err['message'], "n";
            exit;
        } else {
            echo "Conexion exitosa a Oracle!";
        }
    }


    public function ejemConsulta(){

        $conexion= oci_new_connect("RICX","12345","127.0.0.1/xe"); //Conexion a base de datos ORACLE

       // $consulta= "SELECT nom_notario FROM BI1 WHERE nom_notario=?, $Nom_tit"; N
       
        $consulta= "SELECT nom_notario FROM BI1"; // SQL
        $stmt = oci_parse($conexion, $consulta); // Unir conexion con consulta
        oci_execute($stmt); // Ejecutar consulta
        $row = oci_fetch_array($stmt); // Guardar consulta en Array
        return json_encode($row); // Convertir el array en json

        }

}
